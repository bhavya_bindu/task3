import React from 'react';
import axios from 'axios';

class Mobile extends React.Component{
    state={users:[]};

    onFormSubmit=event=>{
        event.preventDefault();
        console.log(this.state.password);
        const user ={
            contact:this.state.contact
        }
        // console.log(user);
        axios.post('/mobile',{data:user})
            .then(res =>{
                console.log("otp:",res.data.otp);
                localStorage.setItem("otp",res.data.otp)
                this.props.history.push("/mobile_verification")
            });
    }
    render(){
        return (
            <div className="container" align="center">
                <h1>Mobile Verification Form</h1>
                <form onSubmit={this.onFormSubmit}>
                    <input type="text" placeholder="enter mobile number" value={this.state.contact} onChange={e=>this.setState({contact: e.target.value})} /><br />
                    <input type="Submit" value="submit"/>
                </form>
            </div>
        );
    }
}

export default Mobile;