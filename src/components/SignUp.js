import React from 'react';
import axios from 'axios';

class SignUp extends React.Component{
    state={users:[]};

    componentDidMount(){
        
    }

    onFormSubmit=event=>{
        event.preventDefault();
        console.log(this.state.password);
        const user ={
            username:this.state.email,
            password:this.state.password,
            isVerified:false
        }
        // console.log(user);
        axios.post('/signup',{data:user})
            .then(res =>{
                console.log("some");
                console.log("signup respose:",res.data.success , res.data.isVerified);
                if(res.data.success && res.data.isVerified)
                {
                    alert("registration success");
                    this.props.history.push("/mobile")
                }
                else if(res.data.success && !res.data.isVerified){
                    alert("please complete the email verification..");
                    this.props.history.push("/signin")
                }
                else{
                    alert("user already exist");
                    this.setState({email:"",password:""})
                }
            });
    }
    render(){
        return (
            <div className="container" align="center">
                <h1>Registration Form</h1>
                <form onSubmit={this.onFormSubmit}>
                    <input type="text" placeholder="enter email" value={this.state.email} onChange={e=>this.setState({email: e.target.value})} /><br />
                    <input type="password" placeholder="enter password" value={this.state.password} onChange={e=>this.setState({password: e.target.value})} /><br />
                    <input type="submit" value="submit"/>
                </form>
            </div>
        );
    }
}

export default SignUp;