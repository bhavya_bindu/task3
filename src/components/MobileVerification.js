import React from 'react';

class MobileVerification extends React.Component{
    state={users:[]};

    onFormSubmit=event=>{
        event.preventDefault();
        const user ={
            otp:this.state.otp
        }
        // console.log(user);
        // console.log(localStorage.getItem('otp'));
        if(user.otp===localStorage.getItem('otp')){
            alert("sucessfully verified");
            this.props.history.push("/signin")
            localStorage.removeItem('otp');
        }
        else{
            alert("enter valid otp");
            this.setState({otp:''});
        }
    }
    render(){
        return (
            <div className="container" align="center">
                <h1>Mobile Verification Form</h1>
                <form onSubmit={this.onFormSubmit}>
                    <input type="text" placeholder="enter OTP" value={this.state.otp} onChange={e=>this.setState({otp: e.target.value})} /><br />
                    <input type="Submit" value="submit"/>
                </form>
            </div>
        );
    }
}

export default MobileVerification;