import React from 'react';
import axios from 'axios';

class SignIn extends React.Component{
    state={email:'', password:''};
    onFormSubmit=event=>{
        event.preventDefault();

        const user ={
            username:this.state.email,
            password:this.state.password
        }

        axios.post('/signin',{data:user})
        .then(res =>{
            console.log("login:",res)
            if(res.data.success && res.data.isVerified){ 
                console.log("signin respose:",res);
                localStorage.setItem('jwtToken',res.data.token);
                console.log(localStorage);
                this.props.history.push('/update');
            }
            else if(!res.data.isVerified){
                alert("please complete your email verification..");
            } 
            else{
                alert("invalid credentials");
                this.setState({email:"",password:""})
            }           
        });
        
    }
    render(){
        return (
            <div className="container" align="center">
                <h1>SignIn Form</h1>
                <form onSubmit={this.onFormSubmit}>
                    <input type="text" placeholder="enter email" value={this.state.email} onChange={e=>this.setState({email: e.target.value})} /><br />
                    <input type="password" placeholder="enter password" value={this.state.password} onChange={e=>this.setState({password: e.target.value})} /><br />
                    <input type="submit" value="submit"/>
                </form>
            </div>
        );
    }
}

export default SignIn;