import React from 'react';
import axios from 'axios';

class Update extends React.Component{
    state={fn:'', ln:'', cnct:'', address:''};
    onFormSubmit=event=>{
        event.preventDefault();

        const user ={
            first_name:this.state.fn,
            last_name:this.state.ln,
            contact:this.state.cnct,
            address:this.state.address
        }

        // console.log(localStorage.jwtToken);
        const token=localStorage.jwtToken;
        axios.post('/update',{user,token})
            .then(res =>{
                console.log("update respose:",res);
                this.props.history.push("/delete")
            });
        
    }
    render(){
        return (
            <div className="container" align="center">
                <h1>Update Form</h1>
                <form onSubmit={this.onFormSubmit}>
                    <input type="text" placeholder="enter first name" value={this.state.fn} onChange={e=>this.setState({fn: e.target.value})} /><br />
                    <input type="text" placeholder="enter last name" value={this.state.ln} onChange={e=>this.setState({ln: e.target.value})} /><br />
                    <input type="text" placeholder="enter contact" value={this.state.cnct} onChange={e=>this.setState({cnct: e.target.value})} /><br />
                    <input type="text" placeholder="enter Address" value={this.state.address} onChange={e=>this.setState({address: e.target.value})} /><br />
                    <input type="submit" value="submit"/>
                </form>
            </div>
        );
    }
}

export default Update;