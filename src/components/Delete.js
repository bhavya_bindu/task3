import React from 'react';
import axios from 'axios';

class SignUp extends React.Component{

    onFormSubmit=event=>{
        event.preventDefault();
        const token = localStorage.jwtToken;
        console.log(token);
        axios.delete('/delete',{
            data:{
                token: localStorage.jwtToken
            }
        })
        .then(res =>{
            if(res.data.success){
                console.log("delete:",res);
                alert("Deleted successfully");
                console.log(localStorage.removeItem('jwtToken'));
                this.props.history.push("/signin")
            }
            else{
                alert("already deleted");
                this.props.history.push("/signin")
            }
        });
    }
    render(){
        return (
            <div className="container" align="center">
                <h1>Delete profile</h1>
                <form onSubmit={this.onFormSubmit}>
                    <input type="submit" value="Delete"/>
                </form>
            </div>
        );
    }
}

export default SignUp;