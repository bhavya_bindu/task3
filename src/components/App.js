import React from 'react';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import SignUp from './SignUp';
import SignIn from './SignIn';
import Update from './Update';
import Delete from './Delete';
import Mobile from './Mobile';
import MobileVerification from './MobileVerification';

class App extends React.Component{
    render(){
        return (
            <Router>
            <div>
                <Switch>
                    <Route exact path='/signup' component={SignUp} />
                    <Route exact path='/signin' component={SignIn} />
                    <Route exact path='/update' component={Update} />
                    <Route exact path='/delete' component={Delete} />
                    <Route exact path='/mobile' component={Mobile} />
                    <Route exact path='/mobile_verification' component={MobileVerification} />
                    {/* <Route path="/verify" component={SignIn} /> */}
                </Switch>
             
            </div>
            </Router>
        );
    }
}

export default App;