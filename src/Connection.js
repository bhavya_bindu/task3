const mongoose = require('mongoose');
const connection = () =>{
    mongoose.connect('mongodb://localhost:27017/task3');

    mongoose.connection.once("open",function(){
        console.log("connection made");
    }).on('error',function(error){
        console.log("connection error");
    });
}

module.export = connection;