const express  = require('express')
const app = express()
var cors = require('cors')
const config =  require('./config')
const chalk = require('chalk')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const responseTime = require('response-time')
const mongoose = require('mongoose');

app.use(cors())

app.use(morgan('combined'))

app.use(bodyParser.json())

app.use(responseTime())

mongoose.connect('mongodb://localhost:27017/myapp', {useNewUrlParser: true});

mongoose.Promise = global.Promise;

var db = mongoose.connection;

db.on('error',function(){
    console.log(chalk.red('Error while conneting to database'))
})

db.on('open',function(){
    console.log(chalk.green('connected to database successfully'))
})

require('./models/User')
require('./routes/router')(app)

app.listen(config.port,function(hi){
    console.log(chalk.green('server is starting on ',config.port))
})