var middleware = require('../middleware/middleware')
var auth_controller = require('../controllers/auth.controller')
var user_controller = require('../controllers/user.controller')

module.exports = function(app) {
    app.post('/signup',auth_controller.signup)

    app.post('/signin',auth_controller.signin)

    app.post('/update',middleware.isauth,user_controller.update)

    app.post('/mobile',user_controller.mobile)

    app.delete('/delete',middleware.isauth,user_controller.delete)
    
    app.get('/verify',user_controller.verify)
}