var mongoose = require('mongoose');
var schema = mongoose.Schema;

var UserSchema = new schema({

    username:String,
    password:String,
    salt:String,
    first_name:String,
    last_name:String,
    contact:String,
    address:String,
    isVerified:Boolean
});

module.exports = mongoose.model('Userschema',UserSchema);