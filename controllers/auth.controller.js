var mailer = require('nodemailer');
var Userschema = require('../models/User')
var middleware = require('../middleware/middleware');
var smtpTransport = mailer.createTransport({
    service: "Gmail",
    auth: {
        user: "bhavyabindhumetla@gmail.com",
        pass: "mohanrao"
    }
});

//user registration
exports.signup = function(req,res){
    data={
        email:req.body.data.username
    }
    var isVerified = req.body.data.isVerified;
    console.log("data is:",data)
    middleware.generateToken(data,23,function(err,token){
        if(!token) {
            console.log("not generated");
        }
        else {
            console.log("generated token",token);
            host=req.get('host');
            link="http://"+req.get('host')+"/verify?id="+token;
            console.log("link ",link);
            var mail = {
                from: "Bhavya Bindu Metla <bhavyabindhumetla@gmail.com>",
                to: req.body.data.username,
                subject : "Please confirm your Email account",
                html : "Hello,<br> Please Click on the link to verify your email.<br><a href="+link+">Click here to verify</a>"	
            }

            Userschema.findOne({"username":req.body.data.username},function(err,result){
                if(!result){
                    smtpTransport.sendMail(mail, function(error, response){
                        // console.log("smtp res :",response);
                        if(error){
                            console.log(error);
                        }else{
                            console.log("Message sent ");
                        }
                    
                        smtpTransport.close();
                    });

                    middleware.generateSalt(function(err,salt){
                        // console.log(salt);
                        middleware.generateHash(req.body.data.password,salt,function(err,hash){
                            // console.log("password: ",req.body.data.password,"username: ",req.body.data.username);
                            if(hash) {
                                // console.log(req.body);
                                req.body.salt = salt
                                req.body.password = hash
                                req.body.username = req.body.data.username
                                var userdata = new Userschema(req.body)
                                console.log(userdata);
                                userdata.save(function(err){
                                    if(err) {
                                        res.json({
                                            "status":"Ok",
                                            "success":false,
                                            "reason":err,
                                            "isVerified":isVerified
                                        })
                                    }
                                    else {
                                        res.json({
                                            "status":"ok",
                                            "success":true,
                                            "isVerified":isVerified
                                        })
                                    }
                                })
                            }
                            
                        })
                    })  
                }
                else{
                    res.json({
                        "status":"no",
                        "success":false,
                        "isVerified":isVerified
                    })
                }
            })
        }
    })
}

//user email and password verifications
exports.signin = function(req,res){
    if(req.body) {
        Userschema.findOne({"username":req.body.data.username},function(err,result){
            // console.log("username",req.body.data.username);
            var isVerified = result.isVerified;
            if(!result) {
                res.json({
                    "status":"Ok",
                    "error":404,
                    "success":false,
                    "response": "No user with this Email"
                })
            }
            else {
                middleware.generateHash(req.body.data.password,result.salt,function(err,hash){
                    // console.log(req.body);
                    if(result.password === hash) {
                        data = {
                            username:result.username,
                            id:result._id
                        }
                        middleware.generateToken(data,23,function(err,token){
                            if(err) {
                                res.json({
                                    "status":"Ok",
                                    "response": "Error while generating token"
                                })
                            }
                            else {
                                res.json({
                                    "status": "Ok",
                                    "success": true,
                                    "token" : token,
                                    "isVerified" : isVerified
                                })
                            }
                        })
                    }
                    else {
                        console.log("invalid");
                        res.json({
                            "success":false
                        })
                    }
                })
            }
        })
    }
}
