var puretext = require('puretext')
var Userschema = require('../models/User')
var middleware = require('../middleware/middleware')

//updating user details
exports.update = function(req,res){
    console.log(req.body.token);
    middleware.decode(req.body.token,function(err,decode){
        console.log(decode)
        if(decode) {
            Userschema.findByIdAndUpdate(decode.data.id,req.body.user ,function(err,update){
                console.log(req.body.user);
                if(err) {
                    res.json({
                        "reason":"error while updating data"
                    })
                }
                else {
                    res.json({
                        "success":true,
                        "message":"successfuly updated messages"
                    })
                }
            })
        }
    })
}


//email verification
exports.verify = function(req,res){
    console.log("request ip :",req.protocol+":/"+req.get('host'));
    if((req.protocol+"://"+req.get('host'))==("http://"+host)){
        console.log("request id:",req.query.id)
        middleware.decode(req.query.id,function(err,decode){
            console.log("decode: ",decode);
            if(decode) {
                data={isVerified:true}
                console.log("isverified data: ",data);
                Userschema.findOneAndUpdate({"username":decode.data.email},{"$set":{isVerified:true}},function(err,result){
                    if(!result) {
                        console.log("not updated")
                    }
                    else {
                        console.log("updated result : ",result);
                        console.log("updated")
                        res.end("<a href='http://192.168.1.70:3000/mobile'>Click here to verify</a>");
                    }
                })
            }
        })
    }
    else
    {
        res.end("<h1>Request is from unknown source");
    }
}

//otp sending
exports.mobile = function(req,res){
    // console.log("mobile number : ",req.body.data.contact);
    var otp = Math.floor(100000 + Math.random() * 900000);

    let text = {
        toNumber: req.body.data.contact,
        fromNumber: '+13166695487',
        smsBody: `Hi,your otp is ${otp}`,
        apiToken: 'qtjh5s'
    }
    puretext.send(text, function (err, response) {
        if (err) {
            console.log(err);
        } else {
            console.log(otp);
            res.json({
                "status": true,
                "otp": otp
            });
        }
    })
}

//deleting account
exports.delete = function(req,res) {
    console.log("delete",req.body.token);
    middleware.decode(req.body.token,function(err,decode){
        console.log(decode);
        if(decode) {
            Userschema.findOneAndRemove({"_id":decode.data.id},function(err,deletes){
                if(!deletes) {
                    res.json({
                        "reason":"error while deleting",
                        "error":err,
                        "success":false
                    })
                }
                else {
                    res.json({
                        "success":true,
                        "message":"successfuly deleted",
                        "resp":deletes
                    })
                }
            })
        }
    })
}